import { Link, Form, redirect } from "remix";
import type { ActionFunction, LinksFunction } from "remix";
import { db } from "../../../db";
import styles from "~/styles/comments.css";

export let links: LinksFunction = () => {
  return [{ rel: "stylesheet", href: styles }];
};

export let action: ActionFunction = async ({ params, request }) => {
  const projectId = parseInt(params.id ?? "");
  const formData = await request.formData();
  const content = formData.get("content") as string;
  console.log({ content, projectId });

  const project = await db.project.findFirst({ where: { id: projectId } });

  if (!project) {
    return redirect("/");
  }

  await db.comment.create({ data: { projectId: project.id, content } });

  return redirect(`/projects/${project.id}`);
};

export default function NewComment() {
  return (
    <div className="comments">
      <h2>New Comment</h2>

      <Form method="post">
        <label>
          <div>Content:</div>
          <textarea name="content" required />
        </label>

        <div>
          <button type="submit">Add</button>
        </div>
      </Form>

      <p>
        <Link to="..">
          <button>Cancel</button>
        </Link>
      </p>
    </div>
  );
}

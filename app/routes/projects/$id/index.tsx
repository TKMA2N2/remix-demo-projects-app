import { useLoaderData, Link } from "remix";
import type { LoaderFunction, LinksFunction } from "remix";
import { db } from "~/db";
import type { Comment } from "@prisma/client";
import styles from "~/styles/comments.css";

type LoaderData = {
  comments: Comment[];
};

export let links: LinksFunction = () => {
    return [{ rel: "stylesheet", href: styles }];
  }

export let loader: LoaderFunction = async ({ params }): Promise<LoaderData> => {
  const projectId = parseInt(params.id ?? "");
  const comments = await db.comment.findMany({ where: { projectId } });

  return {comments};
};

export default function Comments() {
  const {comments} = useLoaderData<LoaderData>();

  return (
    <div className="comments">
      <h3>Comments</h3>
      {comments.length > 0 && (
        <ul>
          {comments.map((comment) => (
            <li key={comment.id}>{comment.content}</li>
          ))}
        </ul>
      )}

      <p>
        <Link to="comment"><button>New Comment</button></Link>
      </p>
    </div>
  );
}

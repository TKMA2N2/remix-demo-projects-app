import type { Project } from ".prisma/client";
import { useEffect, useRef } from "react";
import {
  Form,
  redirect,
  useTransition,
  useLoaderData, 
  useSubmit,
  Outlet
} from "remix";
import type { ActionFunction, LoaderFunction } from "remix";
import { db } from "../../db";

export let loader: LoaderFunction = async ({ params }) => {
  return db.project.findUnique({
    where: {
      id: Number(params.id),
    },
    include: {
      tasks: {
        select: {
          id: true,
          title: true,
          complete: true,
        },
        orderBy: {
          id: "asc",
        },
      },
    },
  });
};

export let action: ActionFunction = async ({ request, params }) => {
  const form = await request.formData();
  console.log(request.method.toLowerCase());
  switch (request.method.toLowerCase()) {
    case "post": {
      const title = form.get("title");
      if (typeof title !== "string") {
        throw new Error(`Form not submitted correctly.`);
      }
      const fields = { title, projectId: Number(params.id) };
      await db.task.create({
        data: fields,
      });
      break;
    }

    case "put": {
      // await new Promise((res) => setTimeout(res, 10000));
      const id = Number(form.get("id"));
      const complete = JSON.parse(String(form.get("complete")));
      
      if (typeof complete !== "boolean") {
        throw new Error(`Form not submitted correctly.`);
      }
      await db.task.update({
        where: {
          id
        },
        data: {
          complete,
        },
      });
    }
  }
  return redirect(`/projects/${params.id}`);
};

export default function Project() {
  let project = useLoaderData<any>();
  let transition = useTransition();
  let ref = useRef<HTMLInputElement>(null);
  let submit = useSubmit();

  useEffect(() => {
    if(transition.state === "submitting"){
      if (ref.current) ref.current.value = "";
    }
  }, [transition.state]);

  return (
    <main>
      <h2>{project.title}</h2>
      <ul>
        {project.tasks.map((task: any) => (
          <li>
            <input
              type="checkbox"
              checked={task.complete}
              onChange={(event) => {
                submit(
                  {
                    complete: String(event.target.checked),
                    id: String(task.id),
                  },
                  {
                    method: "put",
                    replace: true,
                  }
                );
              }}
            />{" "}
            {task.title}
          </li>
        ))}
        {transition.state === "submitting" &&
          transition.submission.method === "PUT" && (
            <li>
              <input type="checkbox" disabled />{" "}
              {transition.submission?.formData.get("title")}
            </li>
          )}
        <li>
          <input type="checkbox" disabled />{" "}
          <Form method="post" replace>
            <input
              ref={ref}
              type="text"
              name="title"
              placeholder="New task"
              aria-label="New task"
            />
          </Form>
        </li>
      </ul>
      <hr />
      <Outlet />
    </main>
  );
}

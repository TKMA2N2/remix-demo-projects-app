import type { ActionFunction, LinksFunction, LoaderFunction } from "remix";
import { useLoaderData, redirect, NavLink, Outlet } from "remix";
import { db } from "~/db";
import styles from "~/styles/projects.css";

export let links: LinksFunction = () => {
  return [{ rel: "stylesheet", href: styles }];
}

// GET
export let loader: LoaderFunction = async () => {
  let projects = await db.project.findMany({
    include: {
      tasks: true,
    },
  });
  return projects;
}

// POST (PUT DELETE PATCH)
export let action: ActionFunction = async ({ request }) => {
  let body = new URLSearchParams(await request.text());
  let project = await db.project.create({
    data: {
      title: body.get("title")!,
    },
  });
  return redirect(`/projects/${project.id}`);
};

export default function Projects() {
  let projects = useLoaderData<any[]>();
  return (
    <section>
      <nav>
        {projects.length === 0 && <p>No projects! Add some ⬇</p>}
        {projects.map((project) => (
          <NavLink to={String(project.id)}>
            {project.title}{" "}
            <small>
              {project.tasks.filter((t: any) => t.complete).length}/
              {project.tasks.length}
            </small>
          </NavLink>
        ))}
        <form method="post" action="/projects">
          <input
            type="text"
            placeholder="Project title"
            aria-label="Project title"
            name="title"
          />
          <button type="submit">Add</button>
        </form>
      </nav>
      <Outlet />
    </section>
  );
}

export function ErrorBoundary({ error }: { error: Error }) {
  console.error(error);
  return (
      <div>
        <h1>There was an error</h1>
        <p>{error.message}</p>
        <hr />
        <p>Something went wrong inside projects route!!!</p>
      </div>
  );
}
